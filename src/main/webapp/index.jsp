<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
    <script type="text/javascript" src="wttc_project_ui/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="wttc_project_ui/js/bootstrap.js"></script>
    <link rel="stylesheet" href="wttc_project_ui/css/bootstrap.css">
    <link rel="stylesheet" href="wttc_project_ui/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="wttc_project_ui/css/style.css">
    <link href='http://fonts.googleapis.com/css?family=Oxygen:700' rel='stylesheet' type='text/css'>
    <title>WTTC</title>
</head>
<body>
<div class="page-header"><img src="http://www.westchestertabletennis.com/images/wttc.jpg"></div>
<div class="text-center">
    <h1 class="left-col">Westchester Table Tennis Center</h1>

    <h2>Recent Tournaments</h2>

    <form name="tournamenttype" action="allEvents" method="get">
        <div class="tournamenslist">
            <select name="tournament">
                <c:forEach var="i" items="${AllTournaments}">
                    <option value="${i.id}">${i.tName}</option>
                </c:forEach>
            </select>
        </div>

        <div class="btn-group-vertical">
            <div class="actions"><label for="new"><input type="radio" name="page" value="new" id="new"
                                                         checked="checked">New</label></div>
            <div class="actions"><label for="edit"><input type="radio" name="page" value="edit"
                                                          id="edit">Edit</label>
            </div>
            <div class="actions"><label><input type="radio" name="page" value="export"
                                               id="export">Export</label></div>
            <div class="actions"><label><input type="radio" name="page" value="delete" id="delete">Delete
            </label></div>
        </div>

        <div class="form-actions"><input type="submit" value="submit" class="btn-success"></div>

    </form>
</div>

<script src="wttc_project_ui/js/app.js"></script>
</body>
</html>