angular.module('contacts', ['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/contact/:index', {
                templateUrl: 'edit.html',
                controller: 'Edit'
            })
            .when('/add', {
                templateUrl: "edit.html",
                controller: "Add"
            })
            .when('/delete/:index', {
                templateUrl: "edit.html",
                controller: "Delete"
            })
            .when('/', {
                templateUrl: 'list.html'
            });
    })
    .controller('Contacts', function ($scope) {
        $scope.contacts = [
            {
                name: 'Maksym',
                number: "+380 66 505 6607"
            },
            {
                name: 'Nadia',
                number: "+380 50 175 2255"
            },
            {
                name: 'Sveta',
                number: "+380 50 842 4409"
            },
            {
                name: 'Marina',
                number: "+380 50 586 0704"
            },
            {
                name: 'Nastia',
                number: "+380 50 980 1009"
            }
        ]
    })
    .controller('Edit', function ($scope, $routeParams) {
        $scope.contact = $scope.contacts[$routeParams.index];
        $scope.index = $routeParams.index;
    })
    .controller('Add', function ($scope, $routeParams) {
        var length = $scope.contacts.push({
            name: 'New Contact',
            number: ''
        });
        $scope.contact = $scope.contacts[length - 1];
        $scope.index = length - 1;
    })
    .controller('Delete', function ($scope, $routeParams, $location) {
        $scope.contacts.splice($routeParams.index, 1);
        $location.path('/').replace();
    });