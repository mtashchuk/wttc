package com.westchestertabletennis.paymentgenerator.dao;

import com.westchestertabletennis.paymentgenerator.dto.DTO;
import com.westchestertabletennis.paymentgenerator.dto.PreparedStatementSetter;
import com.westchestertabletennis.paymentgenerator.dto.RowMapper;
import com.westchestertabletennis.paymentgenerator.dto.TournamentDTO;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Generic methods to read/update db.
 */
public class ConnectionUtil {
    public static List readDTOs(String sql, PreparedStatementSetter setter, RowMapper rowMapper) {
        return (List) doStuff(false, true, sql, setter, rowMapper);
    }

    public static TournamentDTO readDTOsWithCreteria(String sql, PreparedStatementSetter setter, RowMapper rowMapper) {
        return (TournamentDTO) doStuff(false, false, sql, setter, rowMapper);
    }

    public static Integer update(String sql, PreparedStatementSetter setter) {
        return (Integer) doStuff(true, false, sql, setter, null);
    }

    private static Connection getConnection() throws URISyntaxException, SQLException {
        URI dbUri = new URI("postgres://ugbzwuiowkzwjy:B--s8vLbr91afHUJ2eYsEwX5wr@ec2-54-243-44-191.compute-1.amazonaws.com:5432/d6toar43vja3oe");
        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath() + "?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
        return DriverManager.getConnection(dbUrl, username, password);
    }

    private static Object doStuff(boolean isUpdate, boolean returnList, String sql, PreparedStatementSetter setter, RowMapper rowMapper) {


        PreparedStatement pstmt = null;
        Connection conn = null;

        List list = null;
        if (returnList) {
            list = new ArrayList();
        }
        try {
            Class.forName("org.postgresql.Driver");
            Properties properties = new Properties();
            //properties.setProperty("user", "postgres");
            //properties.setProperty("password", "zxcasd");
            //conn = DriverManager.getConnection("jdbc:postgresql://localhost/wttc", properties);
            conn = getConnection();
            pstmt = conn.prepareStatement(sql);
            if (setter != null) {
                setter.doAllStuffNeededToPrepareStatement(pstmt);
            }
            if (isUpdate) {
                return (Integer) pstmt.executeUpdate();
            } else {
                ResultSet rs = pstmt.executeQuery();
                while (rs.next()) {
                    DTO dto = rowMapper.createDTOFromResultSet(rs);
                    if (returnList) {
                        list.add(dto);
                    } else {
                        return dto;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (returnList) {
            return list;
        } else {
            return null;
        }
    }
}
